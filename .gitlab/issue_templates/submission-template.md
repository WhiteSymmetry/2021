<!-- Title format: "Project_Title | Team_Name" -->

# Project Description
<!-- A few paragraphs describing the project as a pitch for others. Tackle domains that relate to quantum computing within the topics mentioned in the JAM page. Do label this issue accordingly! (If you wish to work on any domain that is not mentioned in the Keywords section, please contact us on Discord)--> 
  
# We are looking for:
<!-- What are kind of skill are you looking for in your team members? (Eg: Programmers, Physicists, Artists, etc describe what criteria or skill you are looking for in your team member -->
  
# Current Members: <!-- up to 5 members per team -->
<!-- Indicate full name, Discord username, institution -->
  
# Motivation and Novelty factor
<!-- What is your motivation for this project? What aspect of it is novel or complimentary to something that is existing currently? -->

# Target Outcome
<!-- What's the expected result of your project? What deliverables do you expect from this Jam? -->
  
# Submission | Summary Report 📝
<!-- Short Summary of your project for the judges. Should not exceed 500 words. You can fill this in the later stages of the JAM close to submission -->
To be updated
  
# Repository Link 📝
<!-- Link to your Git repository with the submission as per requirements. -->
To be updated
  
# Presentation Video and PDF link 📝
<!-- Links to your presentation video and PDF. Please make sure they are accessible to the judges. -->
To be updated

<!-- End of Submission. Scroll above and fill up the sections as per the description -->

<!-- (FYI)
######################
Grading rubrics:
- Motivation (15%)
- Originality (15%)
- Success of implementation (20%)
- Outcome (30%)
- Presentation (20%)
######################
-->

<!-- End of Template -->
