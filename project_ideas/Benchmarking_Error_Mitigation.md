# Benchmarking Error Mitigation techniques

**Keywords:** QImplementaion, QProgramming, QSoftware, OpenQEcoSystem

## Motivation
We discussed some basics of near-term error mitigation in the QJAM Talks. This is quite an active area of research, see https://arxiv.org/abs/2011.01382 for a fairly recent review, with extensive references.

A relevant question to ask here is how good are these error mitigation techniques? It would be useful to (perhaps, numerically) benchmark all these various techniques for a variety of algorithms. See https://arxiv.org/abs/2109.04457 for a recent result on performance bounds for such techniques.


## Some Ideas

Try to benchmark a few error mitigation techniques being utilized and gain some insights. Define and justify the metrics you intend to use and also try to extrapolate it to standards that are scalable and extendable to multiple platforms.

Can these near-term error mitigation techniques be combined with textbook error correction techniques? Would these produce better results? See https://arxiv.org/abs/2103.04915 for recent work on this. Not much is known here, so this is fertile ground for new work!

For background on (“textbook”) quantum error correction, you can read Mike & Ike’s famous textbook, or the many freely available introductions online, such as https://arxiv.org/abs/0904.2557


##

