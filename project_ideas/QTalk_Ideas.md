# QTalk Ideas - QJam2021
Ideas from QTalks that took place from Nov 23 - Nov 25, 2021

# Music generation using quantum computer's - Eduardo's talk
Music generation via quantum computer's based on Eduardo's QJam talk:
https://www.youtube.com/watch?v=ihOFT9aYjeI&ab_channel=QWorld

QuTune: https://www.plymouth.ac.uk/research/iccmr/qutune

**Keywords:** QImplementaion, QProgramming, QSoftware

# Platform development for collective explanation - Xavier's Talk
Platform development for collective explanation based of Xavier's QJam talk: An environment to evolve collective explanations: https://youtu.be/MpgHSbndExo

**Keywords:** QImplementaion, QProgramming, QSoftware

# Building better visual explainations - Aurel & Reka's Talk
Building better / visual explanations based on Aurel and Raka's talk on their Quantum shorts video Quing Solomon

Réka Deák and Aurél Gábris - Quing: https://youtu.be/07QBh1cR1Pk  
Quing Solomon Video: https://cqtshorts.quantumlah.org/entry/quing-solomon

**Keywords:** QEducation, QShort, OpenQEcosystem, QOutreach
