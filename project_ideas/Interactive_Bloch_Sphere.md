# Interactive Bloch Sphere

**Keywords:** QEducation, QGames, QProgramming

## Motivation

To teach Bloch sphere with some interactive and visual tools or games.

To develop a tutorial introducing the Bloch sphere and the operations on it.

## Some available examples

**Qubit Bloch Sphere Visualization**  
A collection of visualization tools for the qubit Bloch sphere written in Python.  
https://github.com/cduck/bloch_sphere

**QuTiP's Bloch sphere library**  
The "qutip.Bloch" class (Python), uses Matplotlib to render the Bloch sphere, 
where as qutip.Bloch3d uses the Mayavi rendering engine to generate a more faithful 3D reconstruction of the Bloch sphere.  
https://qutip.org/docs/latest/guide/guide-bloch.html

**Manim (Mathematical Animation Engine)**  
Manim is an engine for precise programmatic animations, designed for creating explanatory math videos.  
https://github.com/3b1b/manim 
 
A [QIntern2020](https://qworld.net/qintern-2020/) project using Manim: https://gitlab.com/qkitchen/openblochsphere

## Ideas

For web-based applications, a JavaScript library/tool can be developed.

Potentially, a Python library can be used by the tutorials based on Jupyter notebooks 
such as [QWorld's Silver](https://qworld.net/qsilver/) or quantum programming frameworks.

A smartphone game can be used during lectures or workshops.

##

_Prepared by Abuzer Yakaryilmaz_
