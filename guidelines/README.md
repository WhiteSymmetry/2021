# Guidelines QJAM2021

This guideline concerns all related information to working on GitLab as a platform for the QJAM 2021 event.  We shall be utilizing this repo as a platform for submissions in the QJAM 2021. Additional information on the submission format and requirements are in the guideline pdf in this directory. Do check them out! 

For any questions or doubts, please ask them in the even discord channel

# Credits:
This document has been designed in [Canva](https://www.canva.com/) using resources from Flaticon.com  
<div>Icons made by <a href="https://www.flaticon.com/authors/konkapp" title="Konkapp">Konkapp</a>, <a href="https://www.freepik.com" title="Freepik">Freepik</a>, and <a href="https://www.flaticon.com/authors/monkik" title="monkik">monkik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
